import re

with open("day2\\input_day2.txt") as file:
    lines = file.readlines()

digits_regex = "(?=(one|two|three|four|five|six|seven|eight|nine|\d))"

bag_contents = {'red': '12', 'green': '13', 'blue': '14'}

def get_game_id(line: str) -> str:
    id_digits = re.findall(digits_regex,line.split(':')[0])
    return ''.join(id_digits)

def get_game(line: str) -> str:
    return ''.join(line.split(':')[1].strip().split(' '))

def get_rounds(game: str) -> list[str]:
    return game.split(';')

def get_num_from_string(string: str) -> str:
    return re.findall('\d+', string)[0]

def get_col_from_string(string: str) -> str:
    return re.findall('(red|green|blue)', string)[0]

def get_power(col_dict: dict[str:int]) -> int:
    power = 1
    for col in col_dict:
        power *= col_dict[col]
    return power


def part_1(text: list[str]):
    total = 0
    for line in text:
        game = get_game(line)
        colours = re.findall('([\d]+[red|green|blue]+)', game)
        print(f"Colours: {colours}")
        possible = True
        for colour in colours:
            num = get_num_from_string(colour)
            col = get_col_from_string(colour)
            if int(bag_contents[col]) < int(num): # not possible
                possible = False
        if possible:
            id = get_game_id(line)
            total += int(id)
    print(f"Part 1 total: {total}")

part_1(lines)

def part_2(text: list[str]):
    total = 0
    for line in text:
        game = get_game(line)
        rounds = get_rounds(game)

        col_max = {'red': 0, 'green': 0, 'blue': 0}
        for round in rounds: # get biggest of each color 
            round_colours = round.split(',')
            for num_colour in round_colours:
                num = get_num_from_string(num_colour)
                col = get_col_from_string(num_colour)
                if int(num) > col_max[col]: # replace the value in dict
                    col_max[col] = int(num)
        total += get_power(col_max)
    print(f"Part 2 total: {total}")

part_2(lines)

