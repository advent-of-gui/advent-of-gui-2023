import re

with open("day4\\input_day4.txt") as file:
    lines = file.readlines()


def get_card_num(line: str) -> str:
    id_digits = re.findall("(\d+)",line.split(':')[0])
    return ''.join(id_digits)

def get_winning_nums(line: str) -> list[str]:
    card_nums = line.split(':')[1]
    win_nums = re.findall("(\d+)",card_nums.split('|')[0])
    return win_nums

def get_my_nums(line:str) -> list[str]:
    card_nums = line.split(':')[1]
    my_nums = re.findall("(\d+)",card_nums.split('|')[1])
    return my_nums

def count_card_wins(win_nums: str, my_nums: str, wins: int=0) -> int:
    for win in win_nums:
        if win in my_nums:
            wins += 1
    return wins


def part_1(text: list[str]) -> int:
    total = 0
    for line in text:
        win_nums = get_winning_nums(line)
        my_nums = get_my_nums(line)
        matches = count_card_wins(win_nums, my_nums, wins=-1)
        if matches >= 0:
            total += (2**matches)
    return total

total = part_1(lines)
print(f"Part 1 solution: {total}")



def part_2(text: list[str]) -> int:
    cards_count = dict()
    for line in text:
        card_num = get_card_num(line)
        cards_count[card_num] = 1

    for i in range(len(text)):
        card_num = get_card_num(text[i])
        card_copies = cards_count[card_num]
        win_nums = get_winning_nums(text[i])
        my_nums = get_my_nums(text[i])
        matches = count_card_wins(win_nums, my_nums, wins=0)
        for j in range(1,matches+1):
            cards_count[str(j+i+1)] += 1*card_copies

    return sum(cards_count.values())

total2 = part_2(lines)
print(f"Part 2 solution: {total2}")

