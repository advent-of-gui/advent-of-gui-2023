from day2 import run_day2 as day2


def test_get_game_id_single_digit():
    expected = '3'
    string = 'Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red'
    id = day2.get_game_id(string)
    assert id == expected

def test_get_game_id_two_digits():
    expected = '33'
    string = 'Game 33: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red'
    id = day2.get_game_id(string)
    assert id == expected

def test_get_game_id_three_digits():
    expected = '333'
    string = 'Game 333: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red'
    id = day2.get_game_id(string)
    assert id == expected

def test_get_game():
    expected = '3blue,4red;1red,2green,6blue;2green'
    game = day2.get_game('Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green')
    assert game == expected

def test_get_num_from_string_one_digit():
    expected = '3'
    num = day2.get_num_from_string('3blue')
    assert num == expected

def test_get_num_from_string_two_digits():
    expected = '32'
    num = day2.get_num_from_string('32blue')
    assert num == expected

def test_get_col_from_string_red():
    expected = 'red'
    col = day2.get_col_from_string('3red')
    assert col == expected

def test_get_col_from_string_blue():
    expected = 'blue'
    col = day2.get_col_from_string('3blue')
    assert col == expected

def test_get_col_from_string_green():
    expected = 'green'
    col = day2.get_col_from_string('3green')
    assert col == expected

def test_get_power_4_2_6():
    expected = 48
    power = day2.get_power({'red': 4, 'green': 2, 'blue': 6})
    assert power == expected

