from day3 import run_day3 as day3

def test_symbol_adjacent_one_row_adjacent():
    data = ['....','.3*.','....']
    is_adjacent = day3.symbol_adjacent(data,0,1)
    assert is_adjacent is True

def test_symbol_adjacent_one_row_not_adjacent():
    data = ['....','.3.*','....']
    is_adjacent = day3.symbol_adjacent(data,0,1)
    assert is_adjacent is False

def test_part_1_one_row_two_nums_one_adjacent():
    data = ['....','3*.3','....']
    total = day3.part_1(data)
    assert total == 3

def test_part_1_one_row_one_2_digit_num_adjacent():
    data = ['....','33*.','....']
    total = day3.part_1(data)
    assert total == 33

def test_part_1_one_row_two_nums_both_adjacent():
    data = ['3*.*4']
    total = day3.part_1(data)
    assert total == 7

def test_part_1_two_rows_one_num_adjacent_vertical_bottom():
    data = ['.3..','.*..']
    total = day3.part_1(data)
    assert total == 3
    
def test_part_1_two_rows_one_num_adjacent_vertical_top():
    data = ['.*..','.3..']
    total = day3.part_1(data)
    assert total == 3

def test_part_1_two_rows_one_num_adjacent_diagonal_bottom():
    data = ['.3..','..*.']
    total = day3.part_1(data)
    assert total == 3

def test_part_1_two_rows_one_num_adjacent_diagonal_top():
    data = ['..*.','.3..']
    total = day3.part_1(data)
    assert total == 3

def test_part_1_two_rows_two_nums_adjacent_to_one_vertical_bottom():
    data = ['.3..4..','.*.....']
    total = day3.part_1(data)
    assert total == 3

def test_part_1_two_rows_two_nums_adjacent_to_both_diagonal_bottom():
    data = ['.3.4..','..*...']
    total = day3.part_1(data)
    assert total == 7

def test_part_1_two_rows_one_num_two_digits_adjacent_to_both_bottom():
    data = ['.34..','.*...']
    total = day3.part_1(data)
    assert total == 34