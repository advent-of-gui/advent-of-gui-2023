import day1.run_day1 as day1


def test_get_digits_from_string_only_numbers():
    expected = ['5','1', '9', '8', '4', '3', '8', '4']
    actual = day1.get_digits_from_string('51sf98asdlfjk43hjg8as4')
    assert actual == expected

def test_get_digits_from_string_only_words():
    expected = ['three', 'two', 'four', 'five']
    actual = day1.get_digits_from_string('threebnakjtwofourjasfive')
    assert actual == expected

def test_get_digits_from_string_words_and_numbers():
    expected = ['5', '1', 'three', 'seven', '3', '8', '2', '4']
    actual = day1.get_digits_from_string('51sfthreedlseven3hjg82as4')
    assert actual == expected

def test_get_digits_from_string_words_with_overlap():
    expected = ['one', 'eight', 'four', 'eight', '5', 'seven']
    actual = day1.get_digits_from_string('sgoneightfoureight5sevenjzsqghg')
    assert actual == expected

def test_convert_words_to_num_four():
    expected = '4'
    actual = day1.convert_word_to_num('four')
    assert actual == expected

def test_get_first_digit_word():
    digits = ['seven', '514', 'three', '987']
    expected = '7'
    actual = day1.get_first_digit(digits)
    assert actual == expected

def test_get_first_digit_single_entry_word():
    digits = ['seven']
    expected = '7'
    actual = day1.get_first_digit(digits)
    assert actual == expected

def test_get_first_digit_single_number():
    digits = ['5', 'three', '987']
    expected = '5'
    actual = day1.get_first_digit(digits)
    assert actual == expected

def test_get_first_digit_single_entry_single_number():
    digits = ['7']
    expected = '7'
    actual = day1.get_first_digit(digits)
    assert actual == expected

def test_get_first_digit_multiple_numbers():
    digits = ['514', 'three', '987']
    expected = '5'
    actual = day1.get_first_digit(digits)
    assert actual == expected

def test_get_first_digit_single_entry_multiple_numbers():
    digits = ['4827']
    expected = '4'
    actual = day1.get_first_digit(digits)
    assert actual == expected

def test_get_last_digit_word():
    digits = ['zero', '514', 'three', 'seven']
    expected = '7'
    actual = day1.get_last_digit(digits)
    assert actual == expected

def test_get_last_digit_single_entry_word():
    digits = ['seven']
    expected = '7'
    actual = day1.get_last_digit(digits)
    assert actual == expected

def test_get_last_digit_single_number():
    digits = ['zero', '514', 'three', '7']
    expected = '7'
    actual = day1.get_last_digit(digits)
    assert actual == expected

def test_get_last_digit_single_entry_single_number():
    digits = ['7']
    expected = '7'
    actual = day1.get_last_digit(digits)
    assert actual == expected

def test_get_last_digit_multiple_numbers():
    digits = ['zero', '514', 'three', '7654']
    expected = '4'
    actual = day1.get_last_digit(digits)
    assert actual == expected

def test_get_last_digit_single_entry_multiple_numbers():
    digits = ['4827']
    expected = '7'
    actual = day1.get_last_digit(digits)
    assert actual == expected

def test_run_day1(input_data):
    expected = 281
    total = day1.main(input_data)
    assert total == expected

