import pytest

@pytest.fixture(scope='session')
def digits_regex():
    yield "one|two|three|four|five|six|seven|eight|nine|[1-9]+"

@pytest.fixture(scope='session')
def digits_dict():
    yield {
    'one': '1',
    'two': '2',
    'three': '3',
    'four': '4',
    'five': '5',
    'six': '6',
    'seven': '7',
    'eight': '8',
    'nine': '9'
}
    
@pytest.fixture(scope='session')
def input_data():
    yield [
        'two1nine',
        'eightwothree',
        'abcone2threexyz',
        'xtwone3four'   ,
        '4nineeightseven2',
        'zoneight234',
        '7pqrstsixteen'
    ]
