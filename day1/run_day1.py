import re


with open("day1\\input_day1.txt") as file:
    lines = file.readlines()

digits_dict = {
    'one': '1',
    'two': '2',
    'three': '3',
    'four': '4',
    'five': '5',
    'six': '6',
    'seven': '7',
    'eight': '8',
    'nine': '9'
}
digits_regex = "(?=(one|two|three|four|five|six|seven|eight|nine|\d))"


def get_digits_from_string(string: str) -> list[str]:
    return re.findall(digits_regex,string)

def convert_word_to_num(string: str) -> str:
    return digits_dict[string]

def get_first_digit(digits: str) -> str:
    if digits[0].isdigit():
        return digits[0][0]
    else:
        return convert_word_to_num(digits[0])
    
def get_last_digit(digits: str) -> str:
    if digits[-1].isdigit():
        return digits[-1][-1]
    else:
        return convert_word_to_num(digits[-1])


def main(text):
    total = 0
    for line in text:
        digits = get_digits_from_string(line)
        first = get_first_digit(digits)
        last = get_last_digit(digits)
        first_last = first + last
        total += int(first_last)
    return total

if __name__ == '__main__':
    total = main(lines)
    print(f"Sum of all calibration values is: {total}")
