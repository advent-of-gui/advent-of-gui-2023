
with open("day3\\input_day3.txt") as file:
    lines = file.readlines()

def surround_data(text: list[str]) -> list[str]:
    textlen = len(text[0])+1
    mydata = ['.'*textlen]
    for line in text:
        mydata.append('.' + line + '.')
    mydata.append('.'*textlen)
    return mydata


def symbol_adjacent(data, row, col) -> bool:
    checks = [[0,-1],[1,0],[1,-1],[-1,0],[-1,-1],[0,1],[-1,1],[1,1]]
    for check in checks:
        cr = row + check[0]
        cc = col + check[1]
        checking: str = data[cr][cc]
        if not checking.isdigit():
            if checking != '.':
                return True
    return False
    


def part_1(text: list[str]) -> int:
    mytext = surround_data(text)
    
    total = 0
    linelen = len(text[0])+1
    # print(linelen)
    for row in range(1,len(mytext)-1):
        isnum = False
        is_adjacent = False
        number = ''
        for col in range(1,linelen):
            # print(f"Row {row}, Col {col}")
            char: str = mytext[row][col]
            if char.isdigit() and col == (linelen-1):
                number += char
                # print(f"number at end of line: {number}")
                if is_adjacent or symbol_adjacent(mytext, row, col):
                    total += int(number)
            if char.isdigit():
                isnum = True
                number += char
            elif isnum: # ending number string and performing checks
                isnum = False
                # print(number)
                if is_adjacent:
                    # print(f"adding {number}")
                    total += int(number)
                    is_adjacent = False
                number = ''
            else:
                number = ''
            if isnum:
                if symbol_adjacent(mytext, row, col):
                    # print(f"Adjacent to Row {row}, Col {col}")
                    is_adjacent = True
    return total

total = part_1(lines)
print(f"Part 1 solution: {total}")

# 211731 -> too low
# 525181 -> Correct
# 525475 -> not right
# 564672 -> too high


def part_2(text):
    pass

# part_2(lines)