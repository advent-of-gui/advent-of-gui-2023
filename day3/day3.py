import sys
from functools import reduce

# add a '.' to each line so we don't have numbers at the end of a line.
# this makes scanning numbers easier.
lines = [s + '.' for s in open("day3\\input_day3.txt").read().strip().split('\n')]

# scan for numbers
in_number = False
nums = {}
print('scanning numbers')
for row, line in enumerate(lines):
    for col, c in enumerate(line):
        if not in_number and c.isdigit():
            col1 = col
            in_number = True
        elif in_number and not c.isdigit():
            num = (row, col1, int(line[col1:col]))
            for col2 in range(col1, col):
                # store the same tuple at every position the number covers
                nums[(row, col2)] = num
            in_number = False

# scan for symbols
print('scanning symbols')
parts = set()
answer1 = 0
answer2 = 0
for row, line in enumerate(lines):
    for col, c in enumerate(line):
        if c.isdigit() or c == '.':
            continue
        gearparts = set()
        # find numbers adjacent to symbol.
        for dr in range(-1, 2):
            for dc in range(-1, 2):
                # It seems that symbols are never on the border of the map,
                # so I'm not checking for that.
                if (row+dr, col+dc) in nums:
                    num = nums[(row+dr, col+dc)]
                    parts.add(num)
                    if c == '*':
                        gearparts.add(num)
        if len(gearparts) == 2:
            answer2 += reduce(lambda a, b: a*b, [p[2] for p in gearparts])
answer1 = sum([p[2] for p in parts])
print(f'answer1 = {answer1}')
print(f'answer2 = {answer2}')